angular.module('noteApp')
.controller('WallController', ['$scope', 'WallService', function($scope, WallService){

    $scope.notes = WallService.fetchNotes();

}]);