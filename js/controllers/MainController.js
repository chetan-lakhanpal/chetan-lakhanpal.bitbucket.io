angular.module('noteApp')
.controller('MainController', ['$scope', '$http', 'ConfSettings', function($scope, $http, ConfSettings){
    $scope.ping = function(type){
        var _url = ConfSettings.api_path + 'ping/' + type;
        $http.post(_url);
    }

    // $scope.ping('website'); 
    
}]);