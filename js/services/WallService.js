angular.module('noteApp')
.service('WallService', ['$http', 'ConfSettings', function($http, ConfSettings){
    this.fetchNotes = function(){
        return [
            {title: 'Hi', content: 'Random data here'},
            {title: 'Heroku', content: 'Not allowing to install DB.'},
            {title: 'Update', content: 'Will update soon'}
            ];
    };
}]);