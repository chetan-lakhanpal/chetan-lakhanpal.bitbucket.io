angular.module('noteApp', ['ui.router', 'angular-loading-bar'])
.constant('ConfSettings', {
    api_path: 'https://chetanlakhanpal-note-app.herokuapp.com/'
})
.config(['$stateProvider', '$urlRouterProvider', '$compileProvider', '$logProvider', function($stateProvider, $urlRouterProvider, $compileProvider, $logProvider){
    
    $urlRouterProvider.otherwise("/home");

    $stateProvider
    .state('header', {
        url: '/header',
        templateUrl: 'views/header.html'
    })
    .state('footer', {
        url: '/footer',
        templateUrl: 'views/footer.html'
    })
    .state('home', {
        url: '/home',
        controller: 'HomeController',
        templateUrl: 'views/home.html'
    })
    .state('wall', {
        url: '/wall',
        controller: 'WallController',
        templateUrl: 'views/wall.html'
    })
    .state('dashboard', {
        url: '/dashboard',
        controller: 'DashboardController',
        templateUrl: 'views/dashboard.html'
    })
    .state('register', {
        url: '/register',
        controller: 'DashboardController',
        templateUrl: 'views/dashboard.html'
    })
    .state('about', {
        url: '/about',
        controller: 'AboutController',
        templateUrl: 'views/about.html'
    });

    $compileProvider.debugInfoEnabled(false);  
}])
.run(function(){
    console.log('Angular Executed');
    (function(){
        if(!navigator.serviceWorker){
            return;
        }
        navigator.serviceWorker.register('/sw.js').then(function(reg){
            console.log(reg);
            console.log('SW Registered.');
        }).catch(function(){
            console.log('SW Failing.');
        });
    })();
});