const gulp = require('gulp');
const notify = require('gulp-notify');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const jsFiles = ['!js/materialize.js', '!js/sw.js', 'js/**/*.js'];
const viewFiles = "views/*.html";

let interceptErrors = function(error) {
  let args = Array.prototype.slice.call(arguments);

  // Send error to notification center with gulp-notify
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>'
  }).apply(this, args);

  // Keep gulp from hanging on this task
  this.emit('end');
};


// This task is used for building production ready
// minified JS/CSS files into the dist/ folder
gulp.task('build', function() {
  const path = './dist';
  let js = gulp.src(jsFiles)
               .pipe(concat('all.min.js'))
               .pipe(gulp.dest(path))
            //    .pipe(rename('all.min.js'))
               .pipe(uglify())
               .pipe(gulp.dest(path))
               .on('error', interceptErrors)
               ;

  return js;
});

gulp.task('default', function() {

  browserSync.init(['./'], {
    server: "./",
    port: 4000,
    notify: true,
    ui: {
      port: 4001
    }
  });

  gulp.watch(jsFiles, ['build', browserSync.reload]);
  gulp.watch('sw.js', browserSync.reload);
});