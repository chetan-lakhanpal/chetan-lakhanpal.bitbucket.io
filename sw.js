var URLS = [
    '/',
    'index.html',
    'views/home.html',
    'views/footer.html',
    'views/about.html',
    'views/dashboard.html',
    'views/wall.html',
    'css/materialize.css',
    'css/style.css',
    'css/animate.min.css',
    'manifest.json',
    'dist/materialize.min.js',
    'dist/wow.min.js',
    'dist/all.min.js',
    'images/img-1.png',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    'https://fonts.googleapis.com/css?family=Roboto+Condensed',
    'https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.9.0/loading-bar.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.1/angular-ui-router.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.9.0/loading-bar.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'
];
var CACHE_NAME = 'note-app-v1';

self.addEventListener('install', function (event) {
    event.waitUntil(
        Promise.all(URLS.map(function (url) {
            return fetch(url, {
                mode: 'no-cors'
            }).then(function (response) {
                if (response.ok) {
                    caches.open(CACHE_NAME).then(function (cache) {
                        cache.put(url, response);
                    })
                }

            }).catch(function (err) {
                console.log(err);
            });
        }))
    );
});

self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.match(event.request).then(function (response) {
            return response || fetch(event.request);
        })
    )
});